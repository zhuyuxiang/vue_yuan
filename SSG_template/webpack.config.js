const path = require('path');

module.exports = {
  mode:'development',//模式
  entry: './src/index.js',//入口
  output: {//出口
    filename: 'bundle.js',
  },
  devServer: {//配置一下webpack-dev-serve
    contentBase: path.join(__dirname,'www'),//静态文件根目录
    compress:false,//是否压缩
    port:8080,//端口号
    publicPath:'/xuni/'
  },
};